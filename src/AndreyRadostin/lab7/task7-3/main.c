#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sym.h"
#define M 256


int main(int argc, char *argv[])
{
	FILE *fp;
	SYM sym[M] = { 0 };
	SYM *symp[M];
	fp = fopen(argv[1], "r");
	
	int chh;
	int symb_c = 0;//count of individual symbols
	int tot_c = 0;//total count

	if (fp == NULL)
	{
		puts("FILE NOT OPEN!!!!!!!");
		return 0;
	}

	while ((chh = fgetc(fp)) != EOF)
	{
		for (int j = 0; j<256; j++)
		{
			if (chh == sym[j].ch)
			{
				sym[j].numb++;
				tot_c++;
				break;
			}
			if (sym[j].ch == 0)
			{
				sym[j].ch = (UC)chh;
				sym[j].numb = 1;
				symb_c++; tot_c++;
				break;
			}
		}
	}
	fclose(fp);

	for (int i = 0; i < symb_c; i++)
	{
		sym[i].freq = (float)sym[i].numb / tot_c;
		symp[i] = sym + i;
	}

	SYM *tempp;
	for (int i = 1; i < symb_c; i++)
	for (int j = 0; j < symb_c - 1; j++)
	if (symp[j]->freq < symp[j + 1]->freq)
	{
		tempp = symp[j];
		symp[j] = symp[j + 1];
		symp[j + 1] = tempp;
	}
	puts("Table");
	for (int i = 0; i < symb_c; i++)
		printName(symp[i]);
	printf("\n");
	return 0;
}

void printName(SYM* item)
{
	if (item != NULL)
	{
		printf("%c \t %f \n", item->ch, item->freq);
	}
}
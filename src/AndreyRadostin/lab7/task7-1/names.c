#include "names.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

PITEM createList(PNAME_REC name_rec)
{
	PITEM item = (PITEM)malloc(sizeof(TITEM));
	item->name_rec = name_rec;
	item->prev = NULL;
	item->next = NULL;
	return item;
}
PNAME_REC createName(char *line)
{
	int i = 0;
	PNAME_REC rec = (PNAME_REC)malloc(sizeof(TNAME_REC));
	while (*line && *line != ',')
		rec->country[i++] = *line++;
	rec->country[i] = 0;
	line++;
	i = 0;
	while (*line && *line != ',')
		rec->region[i++] = *line++;
	rec->region[i] = 0;
	line++;
	i = 0;
	*line++;
	while (*line)
		rec->name[i++] = *line++;
	rec->name[i-2] = 0;
	return rec;
}
PITEM addToTail(PITEM tail,
	PNAME_REC name_rec)
{
	PITEM item = createList(name_rec);
	if (tail != NULL)
	{
		tail->next = item;
		item->prev = tail;
	}
	return item;
}
int countList(PITEM head)
{
	int count = 0;
	while (head)
	{
		count++;
		head = head->next;
	}
	return count;
}
PITEM findByCountry(PITEM head, char *name)
{
	while (head)
	{
		if (_strcmpi(head->name_rec->country, name) == 0)
			return head;
		head = head->next;
	}
	return NULL;
}
PITEM findByName(PITEM head, char *name)
{
	while (head)
	{
		if (_strcmpi(head->name_rec->name, name) == 0)
			return head;
		head = head->next;
	}
	return NULL;
}
void printName(PITEM item)
{
	if (item != NULL)
	{
		puts(item->name_rec->country);
		puts(item->name_rec->region);
		puts(item->name_rec->name);
	}
}

void printCountry(PITEM item0)
{
	PITEM item = item0;
	if (item != NULL)
	{

		puts(item->name_rec->country);

		while (_strcmpi(item0->name_rec->country, item->name_rec->country) == 0)
		{
			puts(item->name_rec->region);
			puts(item->name_rec->name);
			item=item->next;
		}
	}
}
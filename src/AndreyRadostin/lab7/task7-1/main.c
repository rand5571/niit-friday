#define _CRT_SECURE_NO_WARNINGS
#include "names.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	FILE *fp;
	int count = 0;
	char buf[512];
	PITEM head, tail, item;
	fp = fopen("fips10_4.csv", "rt");
	if (!fp) {
		perror("File Fips10_4.csv:");
		exit(1);
	}
	fgets(buf, 512, fp);
	while (fgets(buf, 512, fp))
	{
		if (count == 0)
		{
			head = createList(createName(buf));
			tail = head;
		}
		else
		{
			tail = addToTail(tail, createName(buf));
		}
		count++;
	}
	fclose(fp);
	printf("Total items: %d\n", countList(head));
	item = findByName(head, "Western Australia");

	if (item == NULL)
		printf("Not found!\n");
	else
		printName(item);

	item = findByCountry(head, "FR");

	if (item == NULL)
		printf("Not found!\n");
	else
		printCountry(item);
	return 0;
}

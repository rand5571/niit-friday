//This program counts words in user's line

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 80


int main()
{
	int i = 0, count = 0, inWord = 0, j = 0;
	int wordstart = 0, wordend = 0;
	char str[N];
	printf("Please, print something \n");
	fgets(str, 80, stdin);

	i = strlen(str) - 1;
	str[i + 1] = '\0';
	str[i] = ' ';
	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] != ' ' && inWord == 0)
		{
			count++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	printf("You have entered %d words \n", count);
	
	return 0;
}
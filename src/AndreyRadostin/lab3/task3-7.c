/*This program calculates numbers of identical symbols in a string
and print the Table with descending frequencies */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 80


int main()
{
	int i = 0, j = 0, Numbs[80], count = 1, k = 0,temp2,flag=1;
	char str[N],temp1;
	printf("Please, print something \n");
	fgets(str, 80, stdin);

	i = strlen(str) - 1;
	str[i] = '\0';

	for (i = 0; str[i] != '\0'; ++i)
	{
		j = i + 1;
		while (str[j] != '\0')
		{
			if (str[j] != str[i])
				j++;
			else
			{
				count++;
				for (k = j; str[k] != '\0'; ++k)
					str[k] = str[k + 1];
			}
		}
		Numbs[i] = count;
		count = 1;
	}
	while (flag != 0)
	{
		flag = 0;
		for (i = 0; i < strlen(str) - 1; ++i)
		if (Numbs[i]<Numbs[i+1])
		{
			temp2 = Numbs[i];
			Numbs[i] = Numbs[i + 1];
			Numbs[i + 1] = temp2;
			temp1 = str[i];
			str[i] = str[i + 1];
			str[i + 1] = temp1;
			flag = 1;
		}
	}
	printf("     Table \n");
	printf("Symbol \t Quantity \n");
	for (i = 0; str[i] != '\0'; ++i)
		printf("%4c \t %5d \n", str[i], Numbs[i]);

	printf("\n ");

	return 0;
}
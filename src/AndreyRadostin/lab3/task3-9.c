//This program finds the maximal sequence of identical symbols in user's string
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 80


int main()
{
	int i = 0, j = 0, count = 1, k = 0,max=1;
	char str[N],temp,max_symb;
	printf("Please, print something \n");
	fgets(str, 80, stdin);

	i = strlen(str) - 1;
	str[i] = '\0';
	temp = str[0];
	for (i = 1; str[i] != '\0'; ++i)
	{
		if (str[i] == temp)
				count++;
			else
			{
				if (count > max)
				{
					max = count;
					max_symb = temp;
				}
				temp = str[i];
				count = 1;
			}
	}
	printf("Max length of sequence %d \n",max);
	for (i = 0; i<max; ++i)
		printf("%c", max_symb);

	printf("\n ");

	return 0;
}

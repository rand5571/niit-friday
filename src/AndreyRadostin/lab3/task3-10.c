/*This program removes the word choosen by user from a line*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 80

int get_int(int count);//control of correct input

int main()
{
	int i = 0, count = 0, Num_word = 100, inWord=0,j=0;
	char str[N];
	printf("Please, print some words \n");
	fgets(str, 80, stdin);
	i = strlen(str) - 1;
	str[i] = ' ';
	//calculating of words' number
	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] != ' ' && inWord == 0)
		{
			count++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	printf("Please insert a number of the word You'd like to delete \n");
	Num_word = get_int(count);
	//substituting of the chosen word's letters with spaces
	i = 0;
	count = 0;
	inWord = 0;
	while (count<Num_word)
	{
		if (str[i] != ' ' && inWord == 0)
		{
			count++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	//deleting excess spaces
	j = i-1;
	inWord = 0;
	while (str[j] != ' ')
	{
		str[j] = ' ';
		j++;
	}
	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] != ' ')
		{
			i++; inWord = 0;
		}
		else if (inWord == 0)
		{
			i++; inWord = 1;
		}
		else
		{
			for (j = i; str[j] != '\0'; ++j)
				str[j] = str[j + 1];
		}

	}
	printf("New line \n");
	for (i = 0; str[i] != '\0'; ++i)
		printf("%c", str[i]);

	printf("\n ");
	return 0;
}



int get_int(int count)
{
	int input;
	char ch;
	while (scanf("%d", &input) != 1 || input > count)
	{
		while ((ch = getchar()) != '\n')
			putchar(ch); // exemption from incorrect character
		printf(" Error input!\n ");
		printf(" Please, insert correct number!\n ");

	}
	return input;
}


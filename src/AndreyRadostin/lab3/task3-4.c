/* This program counts summ of number's sequences*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 80
#define Num_Dig 3//number of digits


//summ of one sequence
int SUMM(int N_0, int Num_St, char *str);

int main()
{
	int i = 0, j=0, inWord = 0, wordstart = 0, Num_length=1;
	
	int summ = 0;
	char str[N];
	printf("Please, print something \n");
	fgets(str, 80, stdin);

	i = strlen(str) - 1;
	str[i + 1] = '\0';
	str[i] = ' ';
	i = 0;
	while (str[i] != '\0')
	{
		if ((str[i] >= '0' && str[i] <= '9') && inWord == 0)
			{
				inWord = 1; wordstart = i;
			}
		
		else if ((str[i] >= '0' && str[i] <= '9') && inWord == 1)
			{
				Num_length++;
			}
		else if ((str[i] < '0' || str[i] > '9') && inWord == 1)
			{
				inWord = 0;
				summ += SUMM(Num_length, wordstart, str);
				Num_length = 1;
			}
		++i;
	}
	printf("%d \n", summ);
	return 0;
}

int SUMM(int N_0, int Num_St, char *arr)
{
	int i0, j0;
	int	summ1, summ_l = 0;
	for (i0 = Num_St+N_0 - N_0%Num_Dig; i0 < N_0+Num_St; ++i0)
		summ_l = 10 * summ_l + (arr[i0]-'0');
	summ1 = summ_l;
	
	summ_l = 0;
	for (i0 = Num_St; i0 < Num_St + N_0 - N_0%Num_Dig; i0 += Num_Dig)
	{
		for (j0 = 0; j0 < Num_Dig; ++j0)
		{
			summ_l = 10 * summ_l + (arr[j0 + i0]-'0');
			
		}
		
		summ1 += summ_l;
		summ_l = 0;
	}
	
	return summ1;
}
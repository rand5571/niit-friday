//summ of elements between the first negative one and the last positive one

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 20



int main()
{
	int i = 0, index_pos = 0, index_neg = 0, j=0;

	int summ = 0;
	int arr[N];
	srand(time(0));
	for (i = 0; i < N; ++i)
	{
		arr[i] =rand() % 19-9;
				
		printf("%d ", arr[i]);
	}
	printf("\n");
	i = 0;
	while (arr[i] >= 0)
	{
		i++;
	}
	index_neg = i;
	printf("neg_ind= %d \n", index_neg);
	i = N-1;
	while (arr[i] <= 0)
	{
		i--;
	}
	index_pos = i;
	printf("pos_ind= %d \n", index_pos);
	for (i = index_neg + 1; i < index_pos; ++i)
		summ += arr[i];
	printf("Summ= %d \n",summ);

	
	return 0;
}
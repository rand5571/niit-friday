/*This program prints the chosen word*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 80
void printWord(char *p);
int getWords(char *str, char **p);
int get_int(int count);//control of int input

int main()
{
	int i = 0, count = 0, Num_word=100;
	char str[N], *p[N];
	printf("Please, print some words \n");
	fgets(str, 80, stdin);
	i = strlen(str) - 1;
	str[i] = ' ';
	count = getWords(str, p);
	printf("Please insert a number of the word You'd like to see again \n");
	Num_word = get_int(count);
	printWord(p[Num_word-1]);
	return 0;
}

int getWords(char *str, char **p)
{
	int i = 0, j = 0, inWord = 0;
	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0)
		{
			p[j] = str + i;
			j++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			inWord = 0;
		i++;
	}
	return j+1;
}

int get_int(int count)
{
	int input;
	char ch;
	while (scanf("%d", &input) != 1 || input > count)
	{
		while ((ch = getchar()) != '\n')
			putchar(ch); // exemption from incorrect character
		printf(" Error input! \n ");
		
	}
	return input;
}

void printWord(char *p)
{

	while (*p != ' ')
	{
		putchar(*p++);
	}
	putchar(' ');
}
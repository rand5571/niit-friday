typedef struct sym SYM;
struct sym
{
	unsigned char ch;
	float freq;
	char code[256];
	SYM *left;
	SYM *right;
};

union code
{
	unsigned char chhh;

	struct byte
	{
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;
		unsigned b4 : 1;
		unsigned b5 : 1;
		unsigned b6 : 1;
		unsigned b7 : 1;
		unsigned b8 : 1;
	}byte;
};

SYM *makeTree(SYM *psym[], int k);//This function creates Huffman's tree
void makeCodes(SYM *root);//This function makes new codes
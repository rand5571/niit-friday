#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "compr.h"


SYM *makeTree(SYM *psym[], int k)
{
	SYM *temp;
	temp = (SYM*)malloc(sizeof(SYM));
	temp->freq = psym[k - 1]->freq + psym[k - 2]->freq;
	temp->code[0] = 0;
	temp->left = psym[k - 1];
	temp->right = psym[k - 2];

	if (k == 2)
		return temp;
	else 
	{
		for (int i = 0; i<k; i++)
		if (temp->freq>psym[i]->freq)
		{
			for (int j = k - 1; j>i; j--)
				psym[j] = psym[j - 1];
			psym[i] = temp;
			break;
		}
	}
	return makeTree(psym, k - 1);
}

void makeCodes(SYM *root)
{
	if (root->left)
	{
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}
	if (root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}


int main()
{
	FILE *fp, *fp2, *fp3;

	fp = fopen("lt1.txt", "rb"); 
	fp2 = fopen("temp.101", "wb");
	fp3 = fopen("goal.ctxt", "wb");

	int chh;  // working variable
	int c_un = 0; //count of unique symbols
	int ct = 0; // total count
	int fsize2 = 0;//number of symbols in temp file
	int ts;//tail of temp file
	int quant[256] = { 0 };//array of unique symbols quantity
	SYM symbols[256] = { 0 }; //array of structures
	SYM *psym[256]; //array of pointers
	float ch_sum = 0;//checksum of frequencies
	int mes[8];//������ 0 � 1
	int i, j;
	char SignFile[] = "CHf";
	char TypeFile[] = "txt";
	union code code1;

	
	if (fp == NULL)
	{
		puts("FILE NOT OPEN!!!!!!!");
		return 0;
	}


	//reading and making table of occurrence
	while ((chh = fgetc(fp)) != EOF)
	{
		for (i = 0; i<256; i++)
		{
			if (chh == symbols[i].ch)
			{
				quant[i]++;
				ct++;
				break;
			}
			if (symbols[i].ch == 0)
			{
				symbols[i].ch = (unsigned char)chh;
				quant[i] = 1;
				c_un++; ct++;
				break;
			}
		}
	}

	// Calculating of frequencies
	for (i = 0; i < c_un; i++)
	{
		symbols[i].freq = (float)quant[i] / ct;
		psym[i] = &symbols[i];
	}
		
	//Sorting in descending order
	SYM tempp;
	for (int i = 1; i< c_un; i++)
	for (int j = 0; j< c_un - 1; j++)
	if (symbols[j].freq<symbols[j + 1].freq)
	{
		tempp = symbols[j];
		symbols[j] = symbols[j + 1];
		symbols[j + 1] = tempp;
	}

	for (int i = 0; i<c_un; i++)
	{
		ch_sum += symbols[i].freq;
		printf("Ch= %d\tFreq= %f\t C= %c\t\n", symbols[i].ch, symbols[i].freq, psym[i]->ch, i);
	}
	printf("\n Total symbols = %d\tCheck sum=%f\n", ct, ch_sum);
	printf("\n Symbols = %d\n", c_un);

	SYM *root = makeTree(psym, c_un);//creating Huffman's tree

	makeCodes(root);

	rewind(fp);
	//creating temporary file
	while ((chh = fgetc(fp)) != EOF)
	{
		for (int i = 0; i<c_un; i++)
		if (chh == symbols[i].ch)
			fputs(symbols[i].code, fp2);
	}
	fclose(fp2);

		
	fp2 = fopen("temp.101", "rb");
	//Finding tale
	while ((chh = fgetc(fp2)) != EOF)
		fsize2++;
	ts = fsize2 % 8;

	//creating of header
	fwrite(SignFile, sizeof(SignFile), 1, fp3);
	fwrite(&c_un, sizeof(int), 1, fp3);
	
	for (i = 0; i<c_un; i++)
	{
		fwrite(&symbols[i].ch, sizeof(char), 1, fp3);
		fwrite(&symbols[i].freq, sizeof(float), 1, fp3);
	}
	fwrite(&ts, sizeof(int), 1, fp3);
	fwrite(&ct, sizeof(int), 1, fp3);
	fwrite(TypeFile, sizeof(TypeFile), 1, fp3);
		
	rewind(fp2);

	
	//writing of compressed file
	j = 0;
	for (i = 0; i<fsize2 - ts; i++)
	{
		mes[j] = fgetc(fp2);
		if (j== 7)
		{
			code1.byte.b1 = mes[0] - '0';
			code1.byte.b2 = mes[1] - '0';
			code1.byte.b3 = mes[2] - '0';
			code1.byte.b4 = mes[3] - '0';
			code1.byte.b5 = mes[4] - '0';
			code1.byte.b6 = mes[5] - '0';
			code1.byte.b7 = mes[6] - '0';
			code1.byte.b8 = mes[7] - '0';
			fputc(code1.chhh, fp3);
			j = 0;
		}
		j++;
	}
	//tail
	j = 0;
	for (int i = 0; i <= ts; i++)
	{
		mes[j] = fgetc(fp2);
		if (j == ts)
		{
			code1.byte.b1 = mes[0] - '0';
			code1.byte.b2 = mes[1] - '0';
			code1.byte.b3 = mes[2] - '0';
			code1.byte.b4 = mes[3] - '0';
			code1.byte.b5 = mes[4] - '0';
			code1.byte.b6 = mes[5] - '0';
			code1.byte.b7 = mes[6] - '0';
			code1.byte.b8 = mes[7] - '0';
			fputc(code1.chhh, fp3);
		}
		j++;
	}

	fclose(fp);
	fclose(fp2);
	fclose(fp3);

	return 0;
}
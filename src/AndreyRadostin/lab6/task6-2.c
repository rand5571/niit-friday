//This program calculates maximal Collatz's sequence 
//Maximal sequence of 525 elements achieved for the number 837799

#include <stdio.h>
typedef long long unsigned int LLU;
LLU Collatz(LLU n, LLU count);

int main()
{
	LLU max_number = 2, max_length = 1;
	LLU i;
	for (i = 2; i <= 1000000; ++i)
	{
		if (Collatz(i,0) > max_length)
		{
			max_number = i;
			max_length = Collatz(i,0);
		}
	}
	printf("Maximal sequence %llu achieved for %llu \n", max_length, max_number);
	return 0;
}

LLU Collatz(LLU n,LLU count)
{
		if (n == 1)
		return ++count;
	else
	{
		if (n % 2 == 0)
		{
			Collatz(n / 2,++count);
		}
		else
		{
			Collatz(3 * n + 1,++count);
		}
	}
}
/* This program transforms an integer number to a string
by using of a recursive function*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 80



int Char_Numb(int N_0, int Num_St, char *str);

int main()
{
	int number,j,i;
	char str[N];
	printf("Please, enter an integer number \n");
	scanf("%d", &number);
	j=Char_Numb(number,0, str);
		
	for (i = j; i >= 0;--i)
	{
		printf("%c", *(str + i));
	}
	return 0;
}

int Char_Numb(int N_0, int i, char *str)
{
	if (N_0 < 10)
	{
		*(str + i) = N_0 + '0';
		return i;
	}
	else
	{
		*(str + i) = N_0 % 10+'0';
		Char_Numb((N_0 - (N_0 % 10)) / 10, ++i, str);
	}
}
	
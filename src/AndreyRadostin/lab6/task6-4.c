//This program calculates the sum of elements of array by using
//of traditional and recursive approaches
//Results M=20 t_tr=0.01 s t_rec=0.12 s, M=25 t_tr=0.18 s t_rec=3.9 s
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>


int SUMM_tr(int *arr1, int N0);
int SUMM_rec(int *arr1, int l,int r);

int main()
{
	int N,i;
	int SUMM = 0;
	srand(time(0));
	printf("Please enter an integer number \n");
	scanf("%d", &N);

	int  *arr1 = (int *)malloc((int)pow(2,N)*sizeof(int));
	clock_t t1, t2;

	if (!arr1)
	{
		puts("Memory allocation error!");
		exit(1);
	}
	for (i = 0; i < (int)pow(2, N); ++i)
		*(arr1 + i) = rand() % 201 - 100;
	
	t1 = clock();
	SUMM = SUMM_tr(arr1, (int)pow(2,N));
	t2 = clock();
	printf("Time: %f s\n", (double)(t2 - t1) / CLOCKS_PER_SEC);
	printf("%d \n", SUMM);
	t1 = clock();
	SUMM = SUMM_rec(arr1,0,(int)(pow(2,N)-1));
	t2 = clock();
	printf("Time: %f s\n", (double)(t2 - t1) / CLOCKS_PER_SEC);
	printf("%d \n", SUMM);
	
	
	free(arr1);
	return 0;
}

int SUMM_tr(int *arr1, int N0)
{
	int SUMM = 0,i=0;
	for (i = 0; i < N0; ++i)
		SUMM += *(arr1 + i);
	return SUMM;
}

int SUMM_rec(int *arr1,int l,int r)
{
	int d = (l + r) / 2;
		return (l==r)?arr1[l]:SUMM_rec(arr1,l,d) + SUMM_rec(arr1,d+1,r);

}

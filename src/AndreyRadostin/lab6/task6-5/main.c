//This program measures time of calculation of Fibonacci sequence N-th term


#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int fib(int N);


int main()
{
	int i;
	clock_t t1, t2;
	FILE *out;

	
	out = fopen("output6-5.txt", "w");
	if (out == 0)
	{
		puts("File error!");
		return 1;
	}
	printf("Table \n");
	printf("N \t time,s \n");
	fprintf(out, "N \t time,s \n");
	for (i = 20; i <= 40; ++i)
	{
		t1 = clock();
		fib(i);
		t2 = clock();
		printf("%d \t %f \n", i,(double)(t2 - t1) / CLOCKS_PER_SEC);
		fprintf(out,"%d \t %f \n", i, (double)(t2 - t1) / CLOCKS_PER_SEC);
	}
	
	fclose(out);

	
	return 0;
}

int fib(int N)
{
	if (N == 1 || N == 2)
		return 1;
	else
		return fib(N - 1) + fib(N - 2);
}
//This program draws self-similar figures 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#define N 27

void DRAW(char(*str)[N], int x, int y, int N_r);


int main()
{
	int i = 0, j, x_c = (N - 1) / 2, y_c = (N - 1) / 2, N_r = 3;
	char str[N][N];
	for (j = 0; j < N;++j)
	for (i = 0; i < N; ++i)
		str[j][i] = ' ';
	DRAW(str, x_c, y_c, N_r);
	for (j = 0; j < N; ++j)
	{
		for (i = 0; i < N; ++i)
			printf("%c", str[j][i]);
		printf("\n");
	}
	return 0;
}
void DRAW(char (*str)[N],int x,int y, int N_r)
{	
	if (N_r == 0)
		str[x][y] = '*';
	else 
	{
		DRAW(str, x, y, N_r - 1);
		DRAW(str, x + (int)pow(3, N_r - 1), y, N_r - 1);
		DRAW(str, x - (int)pow(3, N_r - 1), y, N_r - 1);
		DRAW(str, x, y + (int)pow(3, N_r - 1), N_r - 1);
		DRAW(str, x, y - (int)pow(3, N_r - 1), N_r - 1);
	}
}

//This program calculates of Fibonacci sequence's N - th term
//without exponential increasing recursion

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef unsigned long long ULL;
ULL fib_iter(ULL K, ULL M, int N);

ULL fib(int i);


int main()
{
	int i;
	FILE *out;
	out = fopen("output6-6.txt", "w");
	if (out == 0)
	{
		puts("File error!");
		return 1;
	}
	
	for (i = 1; i <= 70; ++i)
	{
		printf("%d \t %lld \t \n", i, fib(i));
		fprintf(out, "%d \t %lld \n", i, fib(i));
	}

	fclose(out);
	return 0;
}
ULL fib(int i)
{
	return fib_iter(0, 1, i);
}

ULL fib_iter(ULL K, ULL M, int N)
{
	if (N == 1)
		return M;
	else
		return fib_iter(M, K + M, N - 1);
}
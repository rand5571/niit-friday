Oh, give us pleasure in the flowers to-day;
And give us not to think so far away
As the uncertain harvest; keep us here
All simply in the springing of the year. 
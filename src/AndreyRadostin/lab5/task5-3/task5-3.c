//This program reads lines from a file, changes the letters' order
//in the middle of each word, and writes its in a file

#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include <string.h>
#include <time.h>
#define M 80

void printWord(char *p, FILE *fp);
int getWords(char *str, char **p);
int main()
{
	char str[M], *p[M];
	int i = 0, j = 0, flag = 1;
	FILE *in, *out;
	srand(time(0));

	in = fopen("input5-3.txt", "r");
	out = fopen("output5-3.txt", "w");
	if (in == 0 || out == 0)
	{
		puts("File error!");
		return 1;
	}

	while (fgets(str, 80, in) != NULL)
	{
		i = strlen(str) - 1;
		str[i] = ' ';

		j = getWords(str, p);

		for (i = 0; i <j; i++)
		{
			printWord(p[i],out);
		}
		fprintf(out,"\n");
				
	}


	fclose(in);
	fclose(out);
	return 0;
}
	
int getWords(char *str, char **p)
{
	int i = 0, j = 0, inWord = 0;
	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0)
		{
			p[j] = str + i;
			j++;
			inWord = 1;
		}
		else if (str[i] == ' ' && inWord == 1)
			
			inWord = 0;
		i++;
	}
	return j;
}
void printWord(char *p, FILE *fp)
{
	char str1[20],temp;
	int i0 = 0, k;
	while (*p != ' ')
	{
		str1[i0] = *p++;
		i0++;
	}
	str1[i0] = '\0';
	if (strlen(str1)>3)
	for (i0 = strlen(str1) - 2; i0 > 0; i0--)
	{
		k = rand() % i0+1;
		temp = str1[i0];
		str1[i0] = str1[k];
		str1[k] = temp;
		
	}
	for (i0 = 0; str1[i0]!='\0'; ++i0)
	{
		putc(str1[i0],fp);
		
	}
	putc(' ',fp);
}

	
//This program reads lines from a file, puts its
//in ascending order of length, and writes its in a file

#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include <string.h>
#define N 25
#define M 80


int main()
{
	char str[N][M], *p[N], *temp;
	int count = 0, i = 0, j = 0, flag = 1;
	FILE *in, *out;
	
	in = fopen("input4-5.txt", "r");
	out = fopen("output4-5.txt", "w");
		if (in == 0 || out == 0)
		{
			puts("File error!");
			return 1;
		}
		
		
	while (count<N && fgets(str[count], 80, in) != NULL)
		p[count] = str[count++];

	for (i = 0; i < count; ++i)
	{
		j = strlen(p[i]) - 1;
		str[i][j] = '\0';
	}
	while (flag != 0)
	{
		flag = 0;
		for (i = 0; i < count - 1; ++i)
		if (strlen(p[i]) > strlen(p[i + 1]))
		{
			temp = p[i];
			p[i] = p[i + 1];
			p[i + 1] = temp;
			flag = 1;
		}
	}
	for (i = 0; i < count; ++i)
		fprintf(out,"%s \n", p[i]);
	fclose(in);
	fclose(out);
	return 0;
}
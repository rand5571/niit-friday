//This program reads user's lines and puts its
//in ascending order of length

#include<stdio.h>
#include <string.h>
#define N 25
#define M 80


int main()
{
	char str[N][M], *p[N], *temp;
	int count = 0, i = 0, j = 0, flag = 1;

	printf("Print some lines \n");
	while (count<N && *fgets(str[count], 80, stdin) != '\n')
		p[count] = str[count++];

	for (i = 0; i < count; ++i)
	{
		j = strlen(p[i]) - 1;
		str[i][j] = '\0';
	}
	while (flag != 0)
	{
		flag = 0;
		for (i = 0; i < count - 1; ++i)
		if (strlen(p[i]) > strlen(p[i + 1]))
		{
			temp = p[i];
			p[i] = p[i + 1];
			p[i + 1] = temp;
			flag = 1;
		}
	}
	for (i = 0; i < count; ++i)
		printf("%s \n", p[i]);

	return 0;
}
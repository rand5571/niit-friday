//This program generates 10 passwords of 8 symbols
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 8


int main()
{
	const int Passw_num = 10;
	int i = 0, j = 0;
	char arr[N];
	srand(time(0));
	for (j = 0; j < Passw_num; ++j)
	{
		for (i = 0; i < N; ++i)
		{
			if (rand() % 3 == 0)
				arr[i] = 'a' + rand() % 26;
			else if (rand() % 3 == 1) arr[i] = '0' + rand() % 10;
			else arr[i] = 'A' + rand() % 26;
			printf("%c ", arr[i]);
		}

		printf("\n");
	}

	return 0;
}
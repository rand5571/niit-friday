//This program assorts letters and numbers
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define N 10


int main()
{
	int i = 0, j=N-1;
	char temp, arr[N];
	srand(time(0));
	for (i = 0; i < N; ++i)
	{
		if (rand() % 2 == 0)
			arr[i] = 'a' + rand() % 26;
		else arr[i] = '0' + rand() % 10;
		printf("%c ", arr[i]);
	}

	printf("\n");
	i = 0;
	while (i != j)
	{
		if ((arr[i] >= 'a' && arr[i] <= 'z') && (arr[j] >= 'a' && arr[j] <= 'z'))
			++i;
		if ((arr[i] >= 'a' && arr[i] <= 'z') && (arr[j] >= '0' && arr[j] <= '9'))
		{
			--j;
			++i;
		}
		if ((arr[i] >= '0' && arr[i] <= '9') && (arr[j] >= '0' && arr[j] <= '9')) 
			--j;
		if ((arr[i] >= '0' && arr[i] <= '9') && (arr[j] >= 'a' && arr[j] <= 'z'))
		{
			temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
			++i;
			--j;
		}
	}
	for (i = 0; i < N; ++i)
		printf("%c ", arr[i]);
	printf("\n");

	return 0;
}
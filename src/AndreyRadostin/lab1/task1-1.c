/* This program advices how to improve user's conditions*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

void result(int b, int c, int d);//function calculating height and weight relation
int get_int();// function controling the integer input
int main()
{
	int height = 0, weight = 0;
	char sex;
	
		printf("Please, enter Your sex: M is for male; F is for female\n");
		scanf("%c", &sex);
		if (sex == 'M' || sex == 'F')
		{
			printf("Please, enter Your height in cm\n");
			height = get_int();
			printf("Please, enter Your weight in kg\n");
			weight = get_int();
			if (sex == 'M') result(height, weight, 100);
			else result(height, weight, 110);

			printf("Thank You!\n");
		}
		else printf("Wrong input\n");
		
	return 0;
}
int get_int()
{
	int input;
	char ch;
	while (scanf("%d", &input) != 1)
	{
		while ((ch = getchar()) != '\n')
			putchar(ch); // exemption from incorrect character
		printf(" is not integer. \n Please, input ");
		printf(" integer number: ");
	}
		return input;
}
void result(int b, int c, int d)
{
	if (b - c < d) printf("You would be better to lose weight.\n");
	else if (b - c == d) printf("Congratulatins! Your weight is normal!\n");
	else printf("You would gain weight better.\n");
}
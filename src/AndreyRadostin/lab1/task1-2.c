/* This program informs about part of a day*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>


int main()
{
	int a = 0, b = 0, c = 0;

	printf("Please, enter current time: hh:mm:ss\n");
	if (scanf("%d:%d:%d", &a, &b, &c) != 3) printf("Wrong input!\n");
	else
	{
		if (a >= 4 && a < 10)  printf("Good morning!\n");
		if (a >= 10 && a < 16) printf("Good afternoon!\n");
		if (a >= 16 && a < 22)  printf("Good evening!\n");
		if (a >= 0 && a < 4) printf("Good night!\n");
		if (a >= 22 && a < 24)  printf("Good night!\n");
	}
	return 0;
}
